require "Theme"
--local currentStage = 0
monsterKillCount = 1

currentTheme = nil
currentMonster = nil

function StageLoad(stage)
  math.randomseed(0)
  currentStage = stage or 0
  
  Themes = {}
  for i = 1, 2 do
    Themes[i] = {}
  end
  --Plains
  Themes[1] = Theme:New(image_background_forest, "Forest", 4) -- int = the number of monsters
  Themes[1].monsters[1] = Enemy:New(5, animation_enemy_bat, fade_enemy_bat, sound_enemy_dead_bat, "Mr. Bat", 750, 360) --monsters
  Themes[1].monsters[2] = Enemy:New(10, animation_enemy_bat, fade_enemy_bat, sound_enemy_dead_bat, "Mr. Bat's Brother-in-law", 750, 360)
  Themes[1].monsters[3] = Enemy:New(7, animation_enemy_ghost, fade_enemy_ghost, nil, "Mrs. Ghost", 750, 360)
  Themes[1].monsters[4] = Enemy:New(6, animation_enemy_rat, fade_enemy_rat, nil, "Mother of rats", 750, 495)
  
  Themes[1].boss = Enemy:New(50, animation_enemy_bat, fade_enemy_bat, sound_enemy_dead_bat, "Mr. Bat's Daddy", 750, 260) --boss
  --Mansion
  Themes[2] = Theme:New(image_background_manion, "Mansion", 3)
  Themes[2].monsters[1] = Enemy:New(2, animation_enemy_bat, fade_enemy_bat, sound_enemy_dead_bat, "Mr. Bat's Sister-in-law", 750, 260)
  Themes[2].monsters[2] = Enemy:New(7, animation_enemy_skeleton, fade_enemy_skeleton, nil, "Skinny Bones", 750, 475)
  Themes[2].monsters[3] = Enemy:New(15, animation_enemy_suitofamour, fade_enemy_suitofamour, nil, "Suit of amour", 750, 480)

  Themes[2].boss = Enemy:New(50, animation_enemy_bat, fade_enemy_bat, sound_enemy_dead_bat, "Mr. Bat's Mummy", 750, 260)

  currentTheme = Themes[1]
  
  currentMonster = currentTheme:GetMonster()
  currentMonster:Show()
  currentTheme.bossActive = false
end

-- TODO: Add a funtion to change stage, also change stages so that they are linear

local ChangeTheme = function()
  math.randomseed(os.time())
  currentTheme = Themes[math.random(1, #Themes)]
end

function Hit(power)
  if (currentMonster ~= nil) then
    currentMonster:Hit(power)
  end
end

function StageUpdate(dt)
  if(currentMonster ~=nil) then
    currentMonster:Update(dt)
    if (currentMonster:IsDead() and currentTheme.bossActive ~= true) then
      currentMonster:Reset()
      currentMonster = currentTheme:GetMonster()
      monsterKillCount = monsterKillCount + 1
      currentStage = currentStage + 1
      if (monsterKillCount % 5 == 0) then
        currentMonster = currentTheme:GetBoss()
        currentTheme.bossActive = true
      end
      currentMonster:Show()
    end
    if (currentTheme.bossActive == true) then
      if (currentTheme:GetTimeLeft(dt) < 0) then
        currentTheme.bossActive = false
		currentMonster:Reset()
        currentMonster = currentTheme:GetMonster()
        currentMonster:Show()
      elseif (currentMonster:IsDead()) then
        currentTheme.bossActive = false
        currentTheme:Reset()
        ChangeTheme()
        currentMonster = currentTheme:GetMonster()
        currentMonster:Show()
      end
    end
  end
end

function StageDraw()
  if(currentTheme ~= nil) then
    currentTheme:Draw()
  end
  if(currentMonster ~=nil) then
    currentMonster:Draw()
  end
end


