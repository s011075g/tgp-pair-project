FadeTexture = {texture = nil, alpha = nil, currentTime = nil}

function FadeTexture:New(texture, o)
  o = o or {}
  
  setmetatable(o, self)
  self.__index = self
  
  o.texture = texture
  o.currentTime = 0
  o.alpha = 255
  
  return o
end

function FadeTexture:Reset()
  self.alpha = 255
  self.currentTime = 0
end

function FadeTexture:Update(dt)
  self.currentTime = self.currentTime + dt
  if (self.currentTime >= 0.05) then
    self.currentTime = self.currentTime - 0.05
    if (self.alpha > 0) then
      self.alpha = self.alpha - 10
    end
  end
end

function FadeTexture:Draw(posX, posY)
  love.graphics.setColor(255,255,255,self.alpha)
  love.graphics.draw(self.texture, posX, posY, 0, 4)
  love.graphics.setColor(255,255,255,255)
end