Animation = {spriteSheet = nil, quads = nil, duration = nil, currentTime = nil, cycleCount = nil}

function Animation:New(image, width, height, duration, o)
  o = o or {}
  
  setmetatable(o, self)
  self.__index = self
  
  o.spriteSheet = image
  o.quads = {}
  
  for y = 0, image:getHeight() - height, height do
    for x = 0, image:getWidth() - width, width do
      table.insert(o.quads, love.graphics.newQuad(x, y, width, height, image:getDimensions()))
    end
  end
  
  o.duration = duration or 1
  o.currentTime = 0
  o.cycleCount = 0
  return o
end

function Animation:Update(dt)
  self.currentTime = self.currentTime + dt
  if (self.currentTime >= self.duration) then
    self.currentTime = self.currentTime - self.duration
    self.cycleCount = self.cycleCount + 1
  end
end

function Animation:Draw(x, y, radian)
  local spriteNum = math.floor(self.currentTime / self.duration * #self.quads) + 1
  if (spriteNum >= 1 and spriteNum <= #self.quads) then --Ensures that it will not crash if it goes over when debugging (will cause the sprite not to draw at times of debugging)
    love.graphics.draw(self.spriteSheet, self.quads[spriteNum], x, y, radian or 0, 4)
  end
end
