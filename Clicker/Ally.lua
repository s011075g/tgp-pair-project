Ally = {posX = nil, posY = nil, animation_still = nil, animation_attack = nil, damage = nil, time = nil, currentTime = nil, animation_state = "", currentAttackCycle = -1, name = "",        damage_level = 1, damage_cost = 10, allyDamageLevel = nil, allyDamagePlus1 = nil, allyDamagePlus10 = nil, allyDamagePlus100 = nil, time_level = 1, time_cost = 10, allyTimePlus1 = nil, allyTimePlus10 = nil, allyTimePlus100 = nil}

function Ally:New(still, attack, damage, time, x, y, name, damage_level, damage_cost, o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  
  o.animation_still = still or nil
  o.animation_attack = attack or nil
  o.damage = damage or 0
  o.time = time or 2
  o.posX = x or 0
  o.posY = y or 0
  
  o.animation_state = "still"
  o.currentTime = 0
  
  o.name = name or ""
  o.damage_level = 1
  o.damage_cost = 10
  
  return o
end

function Ally:Update(dt)
  if (self.animation_state == "still") then
    if (self.animation_still ~= nil) then 
      self.animation_still:Update(dt)
    end
  elseif (self.animation_state == "attack") then
    if (self.animation_attack ~= nil) then 
      self.animation_attack:Update(dt)
      if (self.currentAttackCycle ~= self.animation_attack.cycleCount) then
        self.animation_state = "still"
        self.currentAttackCycle = self.animation_attack.cycleCount
      end
    end
  end
end

function Ally:GetAttackDamageThisUpdate(dt)
  self.currentTime = self.currentTime + dt
  if (self.currentTime >= self.time) then
    self.currentTime = self.currentTime - self.time
    self.animation_state = "attack"
    return self.damage
  end
  return 0
end

function Ally:Draw()
  if (self.animation_state == "still") then
    if (self.animation_still ~= nil) then 
      self.animation_still:Draw(self.posX, self.posY)
    end
  elseif (self.animation_state == "attack") then
    if (self.animation_attack ~= nil) then 
      self.animation_attack:Draw(self.posX, self.posY)
    end
  end
end

--examples of some possible upgrades 
function Ally:UpgradeTime(amount) -- enter negative numbers to decrease time of course 
  self.time = self.time - amount
end

function Ally:UpgradeDamage(amount)
    self.damage = self.damage + amount
end