coin = {lifeTime = 200, posX = nil, posY = nil, texture = nil}

function coin:New(posX, posY, texture, o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  o.posX = posX or 0
  o.posY = posY or 0
  o.texture = texture or nil
  return o
end

function coin:Update(dt)
  if (self.texture ~= nil)
    self.texture:Update(dt)
  end
end

function coin:Draw()
  if (self.texture ~= nil)
    self.texture:Draw()
  end
end

local coins = {}
local tablepos = 1

function LoadCoins()
  coins = {}
end

function CreateCoins(value)
  math.randomseed(os.time)
  for i = 1, value do
    coins[tablepos] = coin:New(math.random(600, 700), 520, animation_item_coin)
    tablepos = tablepos + 1
  end
end

function UpdateCoins(dt)
  for i = 1, tablepos - 1 do
    coins[i]:Update(dt)
  end
end

function DrawCoins()
  for i = 1, tablepos - 1 do
    coins[i]:Draw()
  end
end