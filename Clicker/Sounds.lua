function soundLoad()
  music_state = {off = 0, on = 1}
  SFX_state = {off = 0, on = 1}
  
  music_state = 1
  SFX_state = 1
  
  background_music = love.audio.newSource("Audio/ClickerMusic.wav")
  background_music:setLooping(true)
  background_music:play()
  
  tap_sound = love.audio.newSource("Audio/Tap.wav", "static")
  tap_sound:setVolume(0.5)
  
  invalid_purchase = love.audio.newSource("Audio/InvalidPurchase.wav", "static")
  invalid_purchase:setVolume(0.5)
  
  valid_purchase = love.audio.newSource("Audio/ValidPurchase.wav", "static")
  valid_purchase:setVolume(0.5)
  
  --[[Mansion Enemy Sounds]]--
  sound_enemy_dead_ghost = love.audio.newSource("Audio/GhostDeath.wav")
  sound_enemy_dead_suit_of_armour = love.audio.newSource("Audio/SuitOfArmourDeath.wav")
  sound_enemy_dead_mother_rat = love.audio.newSource("Audio/MotherRatDeath.mp3")
  sound_enemy_dead_butler = love.audio.newSource("Audio/ButlerDeath.mp3")
  sound_enemy_dead_spider= love.audio.newSource("Audio/SpiderDeath.mp3")
  
  --[[Forest Enemy Sounds]]--
  sound_enemy_dead_bat = love.audio.newSource("Audio/BatDeath.wav")
end

function soundUpdate()
  if (music_state == 0) then
    background_music:setVolume(0.0)
  elseif (music_state == 1) then
    background_music:setVolume(1)
  end
  
  if (SFX_state == 0) then
    tap_sound:setVolume(0.0)
    invalid_purchase:setVolume(0.0)
    valid_purchase:setVolume(0.0)
    
    sound_enemy_dead_ghost:setVolume(0.0)
    sound_enemy_dead_suit_of_armour:setVolume(0.0)
    sound_enemy_dead_mother_rat:setVolume(0.0)
    sound_enemy_dead_butler:setVolume(0.0)
    sound_enemy_dead_spider:setVolume(0.0)
    
    sound_enemy_dead_bat:setVolume(0.0)
  elseif (SFX_state == 1) then
    tap_sound:setVolume(0.5)
    invalid_purchase:setVolume(0.5)
    valid_purchase:setVolume(0.5)
    
    sound_enemy_dead_ghost:setVolume(1)
    sound_enemy_dead_suit_of_armour:setVolume(1)
    sound_enemy_dead_mother_rat:setVolume(1)
    sound_enemy_dead_butler:setVolume(1)
    sound_enemy_dead_spider:setVolume(1)
    
    sound_enemy_dead_bat:setVolume(1)
  end
end