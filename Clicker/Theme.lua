Theme = { background = nil, name = nil, monsters = {}, boss = nil, bossActive = false, time = nil, currentTime = nil}

function Theme:New(background, name, monsterCount, bossTime, o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  
  o.background = background or nil
  o.name = name or ""
  o.monsters = {}
  for i = 1, monsterCount or 0 do
    o.monsters[i] = {}
  end
  o.boss = nil
  o.time = bossTime or 10
  o.currentTime = 0
  o.bossActive = false
  math.randomseed(os.time())
  return o
end

function Theme:Draw()
  if (self.background~=nil) then
    love.graphics.draw(self.background)
  end
end

function Theme:Reset()
  self.currentTime = 0
  if (self.boss ~= nil) then self.boss:Reset() end
end

function Theme:GetMonster()
  if(self.monsters == nil) then
    return nil
  end
  return self.monsters[math.random(1, #self.monsters)]
end

function Theme:GetBoss()
  if (self.boss ~= nil) then
    return self.boss
  end
  return nil
end

function Theme:GetTimeLeft(dt)
  self.currentTime = self.currentTime + dt
  return self.time - self.currentTime
end