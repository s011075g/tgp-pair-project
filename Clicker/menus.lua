function menuLoad()
  menu_state = {game = 0, heroes = 1, upgrades = 2, taps = 3, prestige = 4, settings = 5}
  menu_state = 0
  
  gooi.setStyle({showBorder = true})
  
  hero_grid = gooi.newPanel({x = 10, y = 250, w = 700, h = 400, layout = "grid 2x6"}):setGroup("HeroGrid")
  
  for i,v in ipairs(Allies) do
    v.allyDamageLevel = gooi.newLabel({text = "Level = " .. v.damage_cost / 10, group = "HeroGrid"})
    
    v.allyDamagePlus1 = gooi.newButton({text = "+1\n \nCost: " .. v.damage_cost, group = "HeroGrid"}):onRelease(function()
                                                                                                  if (menu_state == 1) then
                                                                                                    if coins >= v.damage_cost then
                                                                                                     v:UpgradeDamage(1)
                                                                                                     coins = coins - v.damage_cost
                                                                                                     v.damage_cost = v.damage_cost + 10
                                                                                                              
                                                                                                     if (valid_purchase:play()) then
                                                                                                      valid_purchase:stop()
                                                                                                      valid_purchase:play()
                                                                                                     else
                                                                                                      valid_purchase:play()
                                                                                                     end
                                                                                                    else
                                                                                                     if (invalid_purchase:play()) then
                                                                                                      invalid_purchase:stop()
                                                                                                      invalid_purchase:play()
                                                                                                     else
                                                                                                      invalid_purchase:play()
                                                                                                     end
                                                                                                    end
                                                                                                  end
                                                                                                end)
    
    v.allyDamagePlus10 = gooi.newButton({text = "+10\n \nCost: " .. v.damage_cost * 10, group = "HeroGrid"}):onRelease(function()
                                                                                                        if (menu_state == 1) then
                                                                                                          if coins >= (v.damage_cost * 10) then
                                                                                                            v:UpgradeDamage(10)
                                                                                                            coins = coins - (v.damage_cost * 10)
                                                                                                            v.damage_cost = (v.damage_cost + 10) * 10
                                                                                        
                                                                                                            if (valid_purchase:play()) then
                                                                                                              valid_purchase:stop()
                                                                                                              valid_purchase:play()
                                                                                                            else
                                                                                                              valid_purchase:play()
                                                                                                            end
                                                                                                          else
                                                                                                            if (invalid_purchase:play()) then
                                                                                                              invalid_purchase:stop()
                                                                                                              invalid_purchase:play()
                                                                                                            else
                                                                                                              invalid_purchase:play()
                                                                                                            end
                                                                                                          end
                                                                                                        end
                                                                                                      end)
                                                                                                    
    v.allyDamagePlus100 = gooi.newButton({text = "+100\n \nCost: " .. v.damage_cost * 100, group = "HeroGrid"}):onRelease(function()
                                                                                                            if (menu_state == 1) then
                                                                                                              if coins >= (v.damage_cost * 100) then
                                                                                                                v:UpgradeDamage(100)
                                                                                                                coins = coins - (v.damage_cost * 100)
                                                                                                                v.damage_cost = (v.damage_cost + 10) * 100
                                                                                                                
                                                                                                                if (valid_purchase:play()) then
                                                                                                                  valid_purchase:stop()
                                                                                                                  valid_purchase:play()
                                                                                                                else
                                                                                                                  valid_purchase:play()
                                                                                                                end
                                                                                                              else
                                                                                                                if (invalid_purchase:play()) then
                                                                                                                  invalid_purchase:stop()
                                                                                                                  invalid_purchase:play()
                                                                                                                else
                                                                                                                  invalid_purchase:play()
                                                                                                                end
                                                                                                              end
                                                                                                            end
                                                                                                          end)
                                                                                                        
    hero_grid:add(gooi.newLabel({text = "", icon = "Textures/UI/" .. v.name .. ".png", group = "HeroGrid"}),
                  gooi.newLabel({text = v.name, group = "HeroGrid"}),
                  v.allyDamageLevel,
                  v.allyDamagePlus1,
                  v.allyDamagePlus10,
                  v.allyDamagePlus100)
  end
             
  upgrade_grid = gooi.newPanel({x = 100, y = 250, w = 700, h = 400, layout = "grid 2x6"}):setGroup("UpgradeGrid")
  
  for i,v in ipairs(Allies) do
    v.allyTimeLevel = gooi.newLabel({text = "Level = " .. v.time_cost / 10, group = "UpgradeGrid"})
    
    v.allyTimePlus1 = gooi.newButton({text = "+1\n \nCost: " .. v.time_cost, group = "UpgradeGrid"}):onRelease(function()
                                                                                                  if (menu_state == 2) then
                                                                                                    if coins >= v.time_cost then
                                                                                                     v:UpgradeTime(1)
                                                                                                     coins = coins - v.time_cost
                                                                                                     v.time_cost = v.time_cost + 10
                                                                                                              
                                                                                                     if (valid_purchase:play()) then
                                                                                                      valid_purchase:stop()
                                                                                                      valid_purchase:play()
                                                                                                     else
                                                                                                      valid_purchase:play()
                                                                                                     end
                                                                                                    else
                                                                                                     if (invalid_purchase:play()) then
                                                                                                      invalid_purchase:stop()
                                                                                                      invalid_purchase:play()
                                                                                                     else
                                                                                                      invalid_purchase:play()
                                                                                                     end
                                                                                                    end
                                                                                                  end
                                                                                                end)
    
    v.allyTimePlus10 = gooi.newButton({text = "+10\n \nCost: " .. v.time_cost * 10, group = "UpgradeGrid"}):onRelease(function()
                                                                                                        if (menu_state == 2) then
                                                                                                          if coins >= (v.time_cost * 10) then
                                                                                                            v:UpgradeTime(10)
                                                                                                            coins = coins - (v.time_cost * 10)
                                                                                                            v.time_cost = (v.time_cost + 10) * 10
                                                                                        
                                                                                                            if (valid_purchase:play()) then
                                                                                                              valid_purchase:stop()
                                                                                                              valid_purchase:play()
                                                                                                            else
                                                                                                              valid_purchase:play()
                                                                                                            end
                                                                                                          else
                                                                                                            if (invalid_purchase:play()) then
                                                                                                              invalid_purchase:stop()
                                                                                                              invalid_purchase:play()
                                                                                                            else
                                                                                                              invalid_purchase:play()
                                                                                                            end
                                                                                                          end
                                                                                                        end
                                                                                                      end)
                                                                                                    
    v.allyTimePlus100 = gooi.newButton({text = "+100\n \nCost: " .. v.time_cost * 100, group = "UpgradeGrid"}):onRelease(function()
                                                                                                            if (menu_state == 2) then
                                                                                                              if coins >= (v.time_cost * 100) then
                                                                                                                v:UpgradeTime(100)
                                                                                                                coins = coins - (v.time_cost * 100)
                                                                                                                v.time_cost = (v.time_cost + 10) * 100
                                                                                                                
                                                                                                                if (valid_purchase:play()) then
                                                                                                                  valid_purchase:stop()
                                                                                                                  valid_purchase:play()
                                                                                                                else
                                                                                                                  valid_purchase:play()
                                                                                                                end
                                                                                                              else
                                                                                                                if (invalid_purchase:play()) then
                                                                                                                  invalid_purchase:stop()
                                                                                                                  invalid_purchase:play()
                                                                                                                else
                                                                                                                  invalid_purchase:play()
                                                                                                                end
                                                                                                              end
                                                                                                            end
                                                                                                          end)
    
    upgrade_grid:add(gooi.newLabel({text = "", icon = "Textures/UI/" .. v.name .. ".png", group = "UpgradeGrid"}),
                     gooi.newLabel({text = v.name, group = "UpgradeGrid"}),
                     v.allyTimeLevel,
                     v.allyTimePlus1,
                     v.allyTimePlus10,
                     v.allyTimePlus100)
  end
                
  tap_grid = gooi.newPanel({x = 475, y = 250, w = 800, h = 400, layout = "grid 2x6"}):setGroup("TapGrid")
  
  tapCost = 10
  tapUpgradeLevel = gooi.newLabel({text = "Level " .. tapCost / 10, group = "TapGrid"})
  tapUpgradePlus1 = gooi.newButton({text = "+1\n \nCost: " .. tapCost, group = "TapGrid"}):onRelease(function()
                                                                                                        if (menu_state == 3) then
                                                                                                          if coins >= tapCost then
                                                                                                            player:UpgradeDamage(1)
                                                                                                            coins = coins - tapCost
                                                                                                            tapCost = tapCost + 10
                                                                                                            
                                                                                                            if (valid_purchase:play()) then
                                                                                                              valid_purchase:stop()
                                                                                                              valid_purchase:play()
                                                                                                            else
                                                                                                              valid_purchase:play()
                                                                                                            end
                                                                                                          else
                                                                                                            if (invalid_purchase:play()) then
                                                                                                              invalid_purchase:stop()
                                                                                                              invalid_purchase:play()
                                                                                                            else
                                                                                                              invalid_purchase:play()
                                                                                                            end
                                                                                                          end
                                                                                                        end
                                                                                                      end)
  
  tapUpgradePlus10 = gooi.newButton({text = "+10\n \nCost: " .. tapCost * 10, group = "TapGrid"}):onRelease(function()
                                                                                                              if (menu_state == 3) then
                                                                                                                if coins >= (tapCost * 10) then
                                                                                                                  player:UpgradeDamage(10)
                                                                                                                  coins = coins - (tapCost * 10)
                                                                                                                  tapCost = (tapCost + 10) * 10
                                                                                                                  
                                                                                                                  if (valid_purchase:play()) then
                                                                                                                    valid_purchase:stop()
                                                                                                                    valid_purchase:play()
                                                                                                                  else
                                                                                                                    valid_purchase:play()
                                                                                                                  end
                                                                                                                else
                                                                                                                  if (invalid_purchase:play()) then
                                                                                                                    invalid_purchase:stop()
                                                                                                                    invalid_purchase:play()
                                                                                                                  else
                                                                                                                    invalid_purchase:play()
                                                                                                                  end
                                                                                                                end
                                                                                                              end
                                                                                                            end)
  
  tapUpgradePlus100 = gooi.newButton({text = "+100\n \nCost: " .. tapCost * 100, group = "TapGrid"}):onRelease(function()
                                                                                                                  if (menu_state == 3) then
                                                                                                                    if coins >= (tapCost * 100) then
                                                                                                                      player:UpgradeDamage(100)
                                                                                                                      coins = coins - (tapCost * 100)
                                                                                                                      tapCost = (tapCost + 10) * 100
                                                                                                                      
                                                                                                                      if (valid_purchase:play()) then
                                                                                                                        valid_purchase:stop()
                                                                                                                        valid_purchase:play()
                                                                                                                      else
                                                                                                                        valid_purchase:play()
                                                                                                                      end
                                                                                                                    else
                                                                                                                      if (invalid_purchase:play()) then
                                                                                                                        invalid_purchase:stop()
                                                                                                                        invalid_purchase:play()
                                                                                                                      else
                                                                                                                        invalid_purchase:play()
                                                                                                                      end
                                                                                                                    end
                                                                                                                  end
                                                                                                                end)
  
  tap_grid:add(gooi.newLabel({text = "", icon = "Textures/UI/tapUpgrade1.png", group = "TapGrid"}),
               gooi.newLabel({text = "Tap Power", group = "TapGrid"}),
               tapUpgradeLevel,
               tapUpgradePlus1,
               tapUpgradePlus10,
               tapUpgradePlus100)
            
  settings_grid = gooi.newPanel({x = 1070, y = 50, w = 200, h = 100, layout = "grid 4x1"}):setGroup("SettingsGrid")
  
  music_check = gooi.newCheck({text = "Music", checked = true, group = "SettingsGrid"})
  SFX_check = gooi.newCheck({text = "SFX", checked = true, group = "SettingsGrid"})
  load_button = gooi.newButton({text = "Load", group = "SettingsGrid"}):onRelease(function()
                                                                                    if (menu_state == 5) then
                                                                                      gooi.confirm({
                                                                                        text = "Are you sure you want to load?",
                                                                                        ok = function()
                                                                                          Load()
                                                                                        end,
                                                                                        cancel = function()
                                                                                          menu_state = 0
                                                                                        end
                                                                                      })
                                                                                    end
                                                                                  end)
                                                                        :warning()
  
  save_button = gooi.newButton({text = "Save", group = "SettingsGrid"}):onRelease(function()
                                                                                    if (menu_state == 5) then
                                                                                      gooi.confirm({
                                                                                        text = "Are you sure you want to save?",
                                                                                        ok = function()
                                                                                          Save()
                                                                                        end,
                                                                                        cancel = function()
                                                                                          menu_state = 0
                                                                                        end
                                                                                      })
                                                                                    end
                                                                                  end)
                                                                        :warning()
  
  settings_grid:add(music_check,
                    SFX_check,
                    load_button,
                    save_button)
                
  gooi.setStyle({showBorder = false})
  
  coin_label = gooi.newLabel({text = "           Coins = " .. coins})
  boss_timer_label = gooi.newLabel({text = "Boss Time: 0"})
  
  game_grid = gooi.newPanel({x = 0, y = 0, w = 1280, h = 720, layout = "game"})
  game_grid:add(gooi.newLabel({text = "", icon = "Textures/UI/coinStill.png"}), "t-l")
  game_grid:add(coin_label, "t-l")
  
  heroes_button = gooi.newButton({text = "Heroes"}):onRelease(function()
                                                              if (menu_state == 1) then
                                                                menu_state = 0
                                                              else
                                                                menu_state = 1
                                                              end
                                                             end)
  
  upgrades_button = gooi.newButton({text = "Upgrades"}):onRelease(function()
                                                              if (menu_state == 2) then
                                                                menu_state = 0
                                                              else
                                                                menu_state = 2
                                                              end
                                                             end)
  
  taps_button = gooi.newButton({text = "Taps"}):onRelease(function()
                                                              if (menu_state == 3) then
                                                                menu_state = 0
                                                              else
                                                                menu_state = 3
                                                              end
                                                             end)
  
  prestige_button = gooi.newButton({text = "Prestige"}):onRelease(function()
                                                                      menu_state = 4
                                                                      gooi.confirm({
                                                                          text = "Are you sure?",
                                                                          ok = function()
                                                                            love.event.quit()
                                                                          end,
                                                                          cancel = function()
                                                                            menu_state = 0
                                                                          end
                                                                      })
                                                                  end)
                                                      :warning()
                                                      
  settings_button = gooi.newButton({text = "", icon = "Textures/UI/settings.png"}):onRelease(function()
                                                                                                  if (menu_state == 5) then
                                                                                                    menu_state = 0
                                                                                                  else
                                                                                                    menu_state = 5
                                                                                                  end
                                                                                             end)
  
  game_grid:add(settings_button, "t-r")
  game_grid:add(gooi.newButton({text = "", icon = "Textures/UI/achievement.png"}), "t-r")
  game_grid:add(boss_timer_label, "t-r")
  game_grid:add(heroes_button, "b-l")
  game_grid:add(upgrades_button, "b-l")
  game_grid:add(prestige_button, "b-r")
  game_grid:add(taps_button, "b-r")
end

function menuUpdate(dt)
  gooi.update(dt)
  coin_label:setText("Coins = " .. coins)
  
  for i,v in ipairs(Allies) do
    v.allyDamageLevel:setText("Level " .. (v.damage_cost/10))
    v.allyDamagePlus1:setText("+1\n \nCost: " .. v.damage_cost)
    v.allyDamagePlus10:setText("+10\n \nCost: " .. v.damage_cost * 10)
    v.allyDamagePlus100:setText("+100\n \nCost: " .. v.damage_cost * 100)
    
    v.allyTimeLevel:setText("Level " .. (v.time_cost/10))
    v.allyTimePlus1:setText("+1\n \nCost: " .. v.time_cost)
    v.allyTimePlus10:setText("+10\n \nCost: " .. v.time_cost * 10)
    v.allyTimePlus100:setText("+100\n \nCost: " .. v.time_cost * 100)
  end
  
  tapUpgradeLevel:setText("Level " .. (tapCost/10))
  tapUpgradePlus1:setText("+1\n \nCost: " .. tapCost)
  tapUpgradePlus10:setText("+10\n \nCost: " .. tapCost * 10)
  tapUpgradePlus100:setText("+100\n \nCost: " .. tapCost * 100)
  
  if (currentTheme.bossActive == true) then
    boss_timer_label:setText("Boss Time: " .. math.floor(currentTheme:GetTimeLeft(dt)))
    boss_timer_label:setVisible(true)
  else
    boss_timer_label:setVisible(false)
  end
  
  if (menu_state == 5) then
    if (music_check.checked) then
      music_state = 1
    else
      music_state = 0
    end
    
    if (SFX_check.checked) then
      SFX_state = 1
    else
      SFX_state = 0
    end
  end
end

function menuDraw()
  love.graphics.setColor(150, 150, 0)
  
	gooi.draw()
  
  if (menu_state == 1) then
    love.graphics.rectangle("fill", hero_grid.x, hero_grid.y, hero_grid.w, hero_grid.h)
    gooi.draw("HeroGrid")
  elseif (menu_state == 2) then
    love.graphics.rectangle("fill", upgrade_grid.x, upgrade_grid.y, upgrade_grid.w, upgrade_grid.h)
    gooi.draw("UpgradeGrid")
  elseif (menu_state == 3) then
    love.graphics.rectangle("fill", tap_grid.x, tap_grid.y, tap_grid.w, tap_grid.h)
    gooi.draw("TapGrid")
  elseif (menu_state == 5) then
    love.graphics.rectangle("fill", settings_grid.x, settings_grid.y, settings_grid.w, settings_grid.h)
    gooi.draw("SettingsGrid")
  end
  
  love.graphics.setColor(255, 255, 255, 255)
end