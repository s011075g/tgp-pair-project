Enemy = {maxhealth = nil, health = nil, healthBar = nil, show = nil, alive = nil, dead = nil, sound = nil, name = nil, posX = nil, posY = nil}

function Enemy:New(health, alive, dead, sound, name, posX, posY, o)
  o = o or {}
  
  setmetatable(o, self)
  self.__index = self
  
  o.maxHealth = health or 1
  o.health = health or 1
  o.alive = alive or nil
  o.dead = dead or nil
  o.sound = sound or nil
  o.name = name
  o.posX = posX or 0
  o.posY = posY or 0
  o.healthBar = gooi.newBar({x = o.posX + 20, y = o.posY - 20, w = 100, h = 20, value = 1})
  o.show = false
  o:HealthBarVisibility(false)
  return o
end

function Enemy:Hit(power)
  if (self.health > 0) then
    self.health = self.health - power
    if (self.health < 0) then self.health = 0 end
    self.healthBar.value = self.health / self.maxHealth
  end

  if (self.health == 0 and self.dead.alpha >= 255) then
    coins = coins + add_coins
    if  (self.sound ~= nil) then self.sound:play() end
  end
end

function Enemy:Update(dt)
  if (self.show == true) then
    if (self.health > 0) then
      if (self.alive ~= nil) then self.alive:Update(dt) end
    else
      if (self.dead ~= nil) then
        if (self.dead.alpha > 15) then
          self.dead:Update(dt)
          self:HealthBarVisibility(false)
        end
      end
    end
  end
end

function Enemy:Reset()
  self.health = self.maxHealth
  if (self.dead ~= nil) then self.dead:Reset() end
  if (self.healthBar ~= nil) then
    self.healthBar.value = 1
    self.show = false
    if (self.healthBar ~= nil) then
      self:HealthBarVisibility(false)
    end
  end
end

function Enemy:IsDead()
  if (self.health <= 0) then
    if(self.dead ~= nil) then
      if (self.dead.alpha < 20) then
        return true
      else
        return false
      end
    return true
    end
  return false
end
end

function Enemy:Show()
  self.show = true
  if (self.healthBar ~= nil) then
    self:HealthBarVisibility(true)
  end
end

function Enemy:Hide()
  self.show = false
  if (self.healthBar ~= nil) then
    self:HealthBarVisibility(false)
  end
end

function Enemy:HealthBarVisibility(bool)
  if (self.healthBar ~= nil) then
    self.healthBar:setVisible(bool)
    self.healthBar:setEnabled(bool)
  end
end

function Enemy:Draw()
  if(self.show == true) then
    if (self.health > 0) then
      if (self.alive ~= nil) then
        self.alive:Draw(self.posX, self.posY)
      end
    else
      if (self.dead ~= nil) then
        self.dead:Draw(self.posX, self.posY)
      end
    end
  end
end