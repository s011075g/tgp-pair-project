local loaded = false;

function SaveData(fileLocation, data)
  if (loaded == false) then
    JSON = assert(loadfile "JSON/JSON.lua")()
    loaded = true
  end
  
  local file = io.open(fileLocation, "w")
  io.output(file)
  io.write(JSON:encode_pretty(data));
  io.close(file)
  
end

function ReadData(fileLocation)
  if (loaded == false) then
    JSON = assert(loadfile "JSON/JSON.lua")()
    loaded = true
  end
  
  local file = assert(io.open(fileLocation, "r"))
  local text = file:read("*all")
  file:close()
  
  local result = JSON:decode(text)
  return result
end