Player = {posX = nil, posY = nil, animation_still = nil, animation_still_hue = nil, animation_attack = nil, animation_attack_hue = nil, animation_state = "", currentAttackCycle = -1, attack_power = nil, hue_color = {}}

function Player:New(posX, posY, attack_power, attack_interval, wait, color, o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  o.posX = posX or 0
  o.posY = posY or 0
  
  o.animation_state = "still"
  o.attack_power = attack_power or 1
  o.attack_interval = attack_interval or 1
  o.wait = o.attack_interval or 1
  o.hue_color = color or {200, 200, 255}
  return o
end

function Player:Update(dt)
  if (self.animation_state == "still") then
    if(self.animation_still ~= nil) then
      self.animation_still:Update(dt)
    end
    if(self.animation_still_hue ~= nil) then
      self.animation_still_hue:Update(dt)
    end
  elseif (self.animation_state == "attack") then
    if(self.animation_attack ~= nil) then
      self.animation_attack:Update(dt)
      if (self.currentAttackCycle ~= self.animation_attack.cycleCount) then
        self.animation_state = "still"
        self.currentAttackCycle = self.animation_attack.cycleCount
      end
    end
    if(self.animation_attack_hue ~= nil) then
      self.animation_attack_hue:Update(dt)
    end
  end
  
  self.wait = self.wait - dt
end

function Player:Draw()
  love.graphics.setColor(255,255,255,255)
  if (self.posX ~= nil and self.posY ~= nil) then
    if (self.animation_state == "still") then
      if (self.animation_still ~= nil) then
        self.animation_still:Draw(self.posX, self.posY);
      end
      if (self.animation_still_hue ~= nil) then
        love.graphics.setColor(self.hue_color[1],self.hue_color[2],self.hue_color[3],255)
        self.animation_still_hue:Draw(self.posX, self.posY);
      end
    elseif (self.animation_state == "attack") then
      if (self.animation_still ~= nil) then
        self.animation_attack:Draw(self.posX, self.posY);
      end
      if (self.animation_still_hue ~= nil) then
        love.graphics.setColor(self.hue_color[1],self.hue_color[2],self.hue_color[3],255)
        self.animation_attack_hue:Draw(self.posX, self.posY);
      end
    end
  end
  love.graphics.setColor(255,255,255,255)
end

function Player:UpgradeDamage(amount)
    self.attack_power = self.attack_power + amount
end

function Player:GetSaveData()
  local data = {}
  data.AttackPower = self.attack_power
  data.AttackInterval = self.attack_interval
  data.Color = self.hue_color
  return data
end

function Player:SetSaveData(data)
  self.attack_power = data.AttackPower
  self.attack_interval = data.AttackInterval
  self.hue_color = data.Color
end