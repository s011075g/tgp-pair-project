function EnemyLoad()
  
  image_enemy_alive_bat = love.graphics.newImage("Textures/Enemies/bat_alive.png")
  image_enemy_dead_bat = love.graphics.newImage("Textures/Enemies/bat_dead.png")
  
  image_enemy_alive_ghost = love.graphics.newImage("Textures/Enemies/ghost_alive.png")
  image_enemy_dead_ghost = love.graphics.newImage("Textures/Enemies/ghost_dead.png")
  
  image_enemy_alive_suitofamour = love.graphics.newImage("Textures/Enemies/suitofamour_alive.png")
  image_enemy_dead_suitofamour = love.graphics.newImage("Textures/Enemies/suitofamour_dead.png")
  
  image_enemy_alive_rat = love.graphics.newImage("Textures/Enemies/rat_alive.png")
  image_enemy_dead_rat = love.graphics.newImage("Textures/Enemies/rat_dead.png")
  
  image_enemy_alive_skeleton = love.graphics.newImage("Textures/Enemies/skeleton_alive.png")
  image_enemy_dead_skeleton = love.graphics.newImage("Textures/Enemies/Skeleton_dead.png")
  
  animation_enemy_bat = Animation:New(image_enemy_alive_bat, 32, 32)
  animation_enemy_ghost = Animation:New(image_enemy_alive_ghost, 32, 32)
  animation_enemy_suitofamour = Animation:New(image_enemy_alive_suitofamour, 32, 32)
  animation_enemy_rat = Animation:New(image_enemy_alive_rat, 32, 32)
  animation_enemy_skeleton = Animation:New(image_enemy_alive_skeleton, 32, 32)
  
  fade_enemy_bat = FadeTexture:New(image_enemy_dead_bat)
  fade_enemy_ghost = FadeTexture:New(image_enemy_dead_ghost)
  fade_enemy_suitofamour = FadeTexture:New(image_enemy_dead_suitofamour)
  fade_enemy_rat = FadeTexture:New(image_enemy_dead_rat)
  fade_enemy_skeleton = FadeTexture:New(image_enemy_dead_skeleton)
end