function AllyLoad()
  image_ally_panda_still = love.graphics.newImage("Textures/Ally/Panda.png")
  image_ally_panda_attack = love.graphics.newImage("Textures/Ally/Panda_attack.png")
  
  animation_ally_panda_still = Animation:New(image_ally_panda_still, 32, 32)
  animation_ally_panda_attack = Animation:New(image_ally_panda_attack, 32, 32, 0.5)
  
  panda = Ally:New(animation_ally_panda_still, animation_ally_panda_attack, 3, 5, 240, 470, "Panda")
  
  image_ally_robot_still = love.graphics.newImage("Textures/Ally/Robot.png")
  image_ally_robot_attack = love.graphics.newImage("Textures/Ally/Robot_attack.png")
  
  animation_ally_robot_still = Animation:New(image_ally_robot_still, 48, 64)
  animation_ally_robot_attack = Animation:New(image_ally_robot_attack, 48, 64, 0.5)
  
  robot = Ally:New(animation_ally_robot_still, animation_ally_robot_attack, 1, 2, 60, 350, "Robot")
  
  Allies = {panda, robot}
end

