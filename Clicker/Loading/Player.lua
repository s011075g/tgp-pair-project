function LoadPlayer()
  
  image_character_spriteSheet = love.graphics.newImage("Textures/Character/hero.png")
  image_character_spriteSheet_hue = love.graphics.newImage("Textures/Character/hero_hue.png")
  image_character_attack_spriteSheet = love.graphics.newImage("Textures/Character/hero_attack.png")
  image_character_attack_spriteSheet_hue = love.graphics.newImage("Textures/Character/hero_attack_hue.png")
  
  animation_character_hero_still = Animation:New(image_character_spriteSheet, 16, 19)
  animation_character_hero_still_hue = Animation:New(image_character_spriteSheet_hue, 16, 19)

  animation_character_hero_attack = Animation:New(image_character_attack_spriteSheet, 27, 19, 0.2)
  animation_character_hero_attack_hue = Animation:New(image_character_attack_spriteSheet_hue, 27, 19, 0.2)
  
  player = Player:New(400, 520)
  player.animation_still = animation_character_hero_still
  player.animation_still_hue = animation_character_hero_still_hue

  player.animation_attack =animation_character_hero_attack
  player.animation_attack_hue = animation_character_hero_attack_hue
  
end