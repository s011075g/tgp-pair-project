function love.conf(t)
	t.window.width = 1280
	t.window.height = 720
	t.title = "Clicker"
	t.externalstorage = true
	t.window.borderless = false 
  t.console = false
end
