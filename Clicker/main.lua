require "gooi"
require "Player"
require "Animation"
require "FadeTexture"
require "Enemy"
require "menus"
require "Stage"
require "Ally"
require "FileIO"
require "Sounds"
require "Loading/Allies"
require "Loading/Enemies"
require "Loading/Player"
require "Loading/Background"
--[[Debug]]--
--require('mobdebug').start()
--require('mobdebug').off()

function love.load()
  love.graphics.setColor(255,255,255,255)
  love.graphics.setDefaultFilter("nearest", "nearest", 1)
  
  soundLoad()
  
  --image_item_coin_spriteSheet = love.graphics.newImage("Textures/Coin.png")
  --animation_item_coin = Animation:New(image_item_coin_spriteSheet, 10, 10)

  ---Tap
  count = 0
  coins = 0
  
  -- When stages are implemented, this variable will be the stage number
  add_coins = 1 -- temp integer
  
  AllyLoad()
  EnemyLoad()
  LoadPlayer()
  LoadBackground()
  menuLoad()
  StageLoad()
  
  --Load()
end

--[[Debug Input]]--
function love.keyreleased(key)
  if key == "escape" then
      love.event.quit()
   end
end

function love.touchreleased(id, x, y, dx, dy, pressure)
  tap()
  gooi.released() 
end

--[[Debug Input]]--
function love.mousepressed(x, y, button)  
  gooi.pressed() 
end

function love.mousereleased(x, y, button, isTouch)
  tap()
  gooi.released() 
end

function tap()
  if (menu_state == 0) then
    
    count = count + 1

    player.animation_state = "attack"
    if (tap_sound:play()) then
      tap_sound:stop()
      tap_sound:play()
    else
      tap_sound:play()
    end
    Hit(player.attack_power)
  end
end

function love.update(dt)
  --[[Object update]]--
  player:Update(dt)

  StageUpdate(dt)
  panda:Update(dt)
  Hit(panda:GetAttackDamageThisUpdate(dt))
  robot:Update(dt)
  Hit(robot:GetAttackDamageThisUpdate(dt))

  --[[Menu update]]--
  menuUpdate(dt)
  
  --[[Sound update]]--
  soundUpdate()
  
  --animation_item_coin:Update(dt)
end

function love.draw()
  love.graphics.setColor(255,255,255,255)
  StageDraw()
  player:Draw()
  panda:Draw()
  robot:Draw()
  menuDraw()
  --animation_item_coin:Draw(40, 40)
end

function Load()
  
end

function Save()
  local info = player:GetSaveData()
  info.Coins = coins
  info.Count = count
  info.DurrentStage = currentStage
  
  SaveData("Files/data.save", info)
end

function Load()
  local info = ReadData("Files/data.save")
  player:SetSaveData(info)
  coins = info.Coins
  count = info.Count
  currentStage = info.DurrentStage 
end